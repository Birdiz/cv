-include docker/.env

CONTAINER ?= frontend

start:
	cd ./docker && ./start.sh

stop:
	cd ./docker && ./stop.sh

restart: stop start

dev:
	cd ./docker && docker-compose exec $(CONTAINER) yarn dev

shell:
	cd ./docker && docker-compose exec $(CONTAINER) sh
