import { defineNuxtConfig } from 'nuxt3'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
    css: ['~/assets/main.css'],

    modules: ['@nuxtjs/color-mode'],

    meta: {
        link: [
            {
                rel: "stylesheet",
                type: "text/css",
                href: "https://use.fontawesome.com/releases/v5.1.0/css/all.css",
                integrity:
                    "sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt",
                crossorigin: "anonymous"
            }
        ],
    }
})
