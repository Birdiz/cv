#!/usr/bin/env bash

set -e

# shellcheck disable=SC2046
readonly DOCKER_PATH=$(dirname $(realpath "$0"))
cd "${DOCKER_PATH}";

# Build all container in parallel to optimize your time
docker-compose up --build
