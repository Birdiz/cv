#!/usr/bin/env bash

set -e

# shellcheck disable=SC2046
readonly DOCKER_PATH=$(dirname $(realpath "$0"))
cd "${DOCKER_PATH}";

docker-compose stop
